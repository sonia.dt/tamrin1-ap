from abc import ABC
p = 3.14


class AshkaleHendesi(ABC):
    def __init__(self, mohit, masahat, t_azla, t_ghotr, hajm):
        self.mohit = mohit
        self.masahat = masahat
        self.t_azla = t_azla
        self.t_ghotr = t_ghotr
        self.hajm = hajm


class TwoDimensional (AshkaleHendesi):
    def __hajm(self):
        print("Nadarad")


class Dayere (TwoDimensional):
    def __init__(self, r):
        self.shoaa = r

    def mohit(self, r):
        self.mohit = 2 * p * r

    def masahat(self, r):
        self.masahat = p*r*r

    def t_azla(self):
        print("binahayat")

    def t_ghotr(self):
        print("binahayat")


class ChandZeli (TwoDimensional):
    def __t_azla(self):
        print("3 ta binahayat")


class ChaharZeli (ChandZeli):
    def __init__(self, a, b, h):
        self.tool = a
        self.arz = b
        self.ertefa = h

    def t_azla(self):
        print("4")


class MotevaziolAzla (ChaharZeli):
    def mohit(self, a, b):
        print((2 * a) + (2 * b))

    def masahat(self, a, b, h):
        print(a * h)

    def t_ghotr(self):
        print("2")


class Mostatil (MotevaziolAzla):
    def __masahat(self, a, b, h):
        if b == h:
            print(a * h)


class Moraba (Mostatil):
    def __mohit(self, a, b):
        if a == b:
            print(4*a)

    def masahat(self, a, b, h):
        if a == b:
            print(a**2)


class Lozi (MotevaziolAzla):
    def __mohit(self, a, b):
        if a == b:
            print(4*a)

    def __masahat(self):
        print("(ghotr_bozorg * ghotr_koochak) / 2 va andaze 2ghotr bastegi be zavie beyne azla darad")


class MosalaseMotesaviolzla (ChandZeli):
    def __init__(self, a):
        self.zel = a

    def mohit(self, a):
        self.mohit(3*a)

    def masahat(self, a):
        self.masahat(((3**1/2) / 2) * (a**2))

    def t_azla(self):
        print("3")

    def t_ghotr(self):
        print("0")


class ThreeDimensional (AshkaleHendesi):
    def __hajm(self):
        print("darad")


class Kore (ThreeDimensional):
    def __init__(self, r):
        self.shoa = r

    def mohit(self):
        print("formuli nadarad")

    def masahat(self, r):
        self.masahat(4 * p * (r**2))

    def t_azla(self):
        print("binahayat")

    def t_ghotr(self):
        print("tarif nashode")

    def hajm(self, r):
        self.hajm((4 // 3) * p * (r ** 3))


class Makhroot (ThreeDimensional):
    def __init__(self, r, h):
        self.shoa = r
        self.ertefa = h

    def mohit(self, r, h):
        print(2 * p * r * h)

    def masahat(self, r, h):
        l = ((r**2) + (h**2)) ** (1/2)
        print((p * (r**2)) + (p * r * l))

    def t_azla(self):
        print("binahayat")

    def t_ghotr(self):
        print("tarif nashode")

    def hajm(self, r, h):
        print(1/3 * p * (r**2) * h)


class MokaabMostatil (ThreeDimensional):
    def __init__(self, a, b, c):
        self.tool = a
        self.arz = b
        self.ertefa = c

    def mohit(self, a, b, c):
        print(4 * a + 4 * b + 4 * c)

    def masahat(self, a, b, c):
        print((2 * a * b) + (2 * a * c) + (2 * b * c))

    def t_azla(self):
        print("12")

    def t_ghotr(self):
        print("2")

    def hajm(self, a, b, c):
        print(a * b * c)


class Mokaab (MokaabMostatil):
    def __mohit(self, a, b, c):
        if a == b == c:
            print(12*a)

    def __masahat(self, a, b, c):
        if a == b == c:
            print(6 * (a ** 2))

    def hajm(self, a, b, c):
        if a == b == c:
            print(a**3)
